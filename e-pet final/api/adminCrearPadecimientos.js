var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var padecimiento = require('../schemas/schemaAdminCrearPadecimientos.js');

router.post('/insertar', function(req, res) {
    var padecimientoNuevo = new padecimiento({
      _id: new mongoose.Types.ObjectId(),
      nombreDelPadecimiento: req.body.nombreDelPadecimiento,
      tipoAnimal: req.body.tipoAnimal
      
    });
  
    padecimientoNuevo.save()
      .then(
        function(result) {
          res.json(result);
        }
    );
});

module.exports = router;  