var express = require('express');
var mongoose = require('mongoose');
/*Inicio Declare este aqui porque era necesario para el multer*/ 
var path = require('path');
/*Fin Declare este aqui porque era necesario para el multer*/ 

var router = express.Router();

var productoOServicio = require('../schemas/schemaProvFormRegProdyServ.js');
var categoriasRegistradas = require('../schemas/adminCrearCategoria.js')


/*Inicio necesario para multer */ 

const multer = require('multer');

let storage = multer.diskStorage({
  destination:(req , file , cb)=>{
    cb(null, './public/imgs/fotosDeCategoriasOServicios')
  },
  filename:(req, file , cb)=>{
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({storage: storage});

router.use(express.urlencoded({extended: true}));
/*Fin necesario para multer*/ 

router.post('/insertar', upload.single('archivo'), function(req, res) {
    
    var tipo = req.body.tipo;
    var tipoTexto = ""

    var especie = req.body.especie;
    var especieTexto = "";

   


    if(tipo == 1){
      tipoTexto = "Servicio"
    } else if(tipo == 2){
      tipoTexto = "Producto"
    } else{
      tipoTexto == ""
    }
        
    if(especie == 1){
      especieTexto = "Perros"
    } else if(especie == 2){
      especieTexto = "Gato"
    } else{
      especieTexto == ""
    }
      
    
  
    
    var productoOServicioNuevo = new productoOServicio({
        _id: new mongoose.Types.ObjectId(),
        nombreDelProducto: req.body.nombreDelProducto,
        descripcion: req.body.descripcion,
        precio: req.body.precio,
        especie: especieTexto,
        tipo: tipoTexto,
        categoria: req.body.categoria,
        nombreFoto : req.file.filename,
        provincias: req.body.provincias,
        cantones:req.body.cantones,
        path: req.file.path,
        identificacionProveedor: req.body.identificacionProveedor
    });
    
    productoOServicioNuevo.save()
      .then(
        function(result) {
          res.json(result);
          
        }
    );
    res.redirect("/html/proveedor/provListProdyServ.html");
    
});



router.get('/categoriasRegistradas', function (req, res) {
  categoriasRegistradas.find().exec()
    .then(
      function (result) {
        res.json(result);
      }
  );
});

module.exports = router;


