var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var proveedores = require('../schemas/proveedorRegProveedor.js');

router.get('/', function(req, res) {
    proveedores.find().exec()
      .then(
        function(result) {
          res.json(result);
        }
      );
  });
  
router.post('/insertar', function(req, res) {
  var proveedoresNuevo = new proveedores({
      _id: new mongoose.Types.ObjectId(),
      cuenta:req.body.cuenta,
      tipoIdEmpresa:req.body.tipoIdEmpresa,
      idEmpresa:req.body.idEmpresa,
      numeroTelefonoProveedor:req.body.numeroTelefonoProveedor,
      provinciaEmpresa: req.body.provinciaEmpresa,
      cantonEmpresa:req.body.cantonEmpresa,
      direccionEmpresa: req.body.direccionEmpresa,
      nombreProveedor:req.body.nombreProveedor,
      primerApellido:req.body.primerApellido,
      segundoApellido:req.body.segundoApellido,
      email: req.body.email,
      tipoIdProveedor:req.body.tipoIdProveedor,
      identificacionProveedor:req.body.identificacionProveedor,
      numeroTelefonoProveedor:req.body.numeroTelefonoProveedor,
      genero:req.body.genero,
      fechaCumple:req.body.fechaCumple,
      contrasena:req.body.pass,
   
  });

  proveedoresNuevo.save()
    .then(
      function(result) {
        res.json(result);
      }
  );
});
module.exports = router;