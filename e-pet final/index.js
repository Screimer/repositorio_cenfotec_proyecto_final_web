var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var app = express();

/*Inicio para el manejo de archivos con multer*/
/*
const multer = require('multer');

let storage = multer.diskStorage({
  destination:(req , file , cb)=>{
    cb(null, './public/imgs/fotosDeCategoriasOServicios')
  },
  filename:(req, file , cb)=>{
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({storage: storage});

app.use(express.urlencoded({extended: true}));
 
app.get('/' , (req, res) =>{
  return res.send("this is the home page");
});

app.post("/subir", upload.single('archivo'), (req , res) => {
  console.log("Storage location is ${req.hostname}/${req.file.path}");
  return res.send(req.file);
  
});
*/

/*Fin para el manejo de archivos con multer*/

// Cambiar por el correcto
/*
try{
  
  console.log(mongoose.Error.messages);
    if(mongoose.Error){
      throw 0;
    } else{
      console.log("Se ha conectado a la base de datos");
    }  
} catch(numE){
  console.log("Error en la conexion a la base de datos, intentelo de nuevo");
}*/

mongoose.connect('mongodb+srv://Screimer:Heliosnxp1516@basedatos.dr8d7.mongodb.net/epet?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});


app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/html/proveedor/provFormRegProdyServ.html', require('./api/provFormRegProdsyServs.js'));

app.use('/html/cliente/clienteListarHistorialPedidos.html' , require('./api/clienteListarHistorialPedidos'));
app.use('/html/administrador/adminListProd.html' , require('./api/adminListProd'));
app.use('/html/administrador/adminListServ.html' , require('./api/adminListServ'));
app.use('/ListaProductos' , require('./api/clienteListaProductosPorCategoria'))
app.use('/clienteDetalleProducto', require('./api/clienteDetalleProd'))

/*Inicio app use de coralia*/ 
app.use('/proveedoresPendientes', require('./api/proveedorRegProveedores.js'));
app.use('/clientes', require('./api/clienteFormRegClientes.js'));

/*Finalizacion de app use de coralia*/

/*Inicio app use de Pablo*/ 


app.use('/proveedores', require('./api/adminListProv.js'));

/*Finalizacion de app use de pablo*/ 



app.listen(8080, function(){
  console.log("Servidor arriba en 8080");
});
