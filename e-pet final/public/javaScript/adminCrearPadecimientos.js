const nombre = document.getElementById("nombre");
const tipo = document.getElementById("tipo");

const advertenciaNombrePadecimiento = document.getElementById("warningNombreDelPadecimiento");
const advertenciaTipoPadecimiento = document.getElementById("warningTipoMascota");

function main(){
    let respuestaFuncionValidacionCampo = validacionCampoNombre();
    let respuestaFuncionValidacionSelectTipo =  validacionSelectTipo();

    if(respuestaFuncionValidacionCampo == false && respuestaFuncionValidacionSelectTipo == false  && terminarRegistro.addEventListener('click', function (e){
        completo = 1; 
        insertarPadecimientosAdministrador(completo);
           
    }) );


}

function validacionCampoNombre(){
    let entrar = false
    let warningCampoNombre = ""

    if(nombre.value.length <= 2 ){
        warningCampoNombre = `Por favor digite un nombre.`
        entrar = true
    }    

    if(entrar == true){
        advertenciaNombrePadecimiento.innerHTML = warningCampoNombre
        
            
    } else{
        advertenciaNombrePadecimiento.innerHTML = ""
    } 

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionSelectTipo(){
    let entrar = false
    let warningSeleccionDeTipo = ""

    if (tipo.value <= 0){
        warningSeleccionDeTipo = `Por favor seleccione un tipo válido.`
        entrar = true
    }
    
    if(entrar) {
        advertenciaTipoPadecimiento.innerHTML = warningSeleccionDeTipo
    } else{
        advertenciaTipoPadecimiento.innerHTML = ""
    }

    if (entrar == false) {
        return false;
    } else{
        return true
    }    
}

function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}


function insertarPadecimientosAdministrador(completo) {
    if(completo == 1 ){
        var datosPadecimientos = {
            nombreDelProducto: document.getElementById("nombre").value,
            tipoAnimal: document.getElementById("tipo").options[tipo.selectedIndex].text
                   
        }
          
            fetch("http://localhost:8080/html/proveedor/adminCrearPadecimientos.html/insertar", {
            method: 'POST',
            body: JSON.stringify(datosPadecimientos),
            headers: {'Content-Type': 'application/json'}
          })
            .then(
              function(response){
                return response.json();
              }
        )
        location.href="provListProdyServ.html"/*Necesito saber a donde tengo que enviar*/ 
    }
    
}