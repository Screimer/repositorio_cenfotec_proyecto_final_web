var filtrado = document.getElementById("filtrado");
let entrar = false;


function cargarTabla() {
    fetch("http://localhost:8080/html/administrador/adminListProd.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {
                for (var cont = 0; json.length > cont; cont++) {
                    if (json[cont].tipo == "Producto") {
                        entrar = true;
                        break;
                    }
                }
                if (entrar == true) {
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            var linea = '<tr class="publicidad1"><td>' + json[cont].nombreContratante + "</td><td>" + json[cont].numeroIdentificacionContratante + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><td>" + json[cont].estado + "</td><td>" + "<i class='fas fa-edit'>" + "</i>" + "</td></tr>"
                            document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                        }

                    }
                } else {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    document.getElementById("warning").innerHTML = "Sin Coincidencias";
                }


            }
        )
}


function cargarTablaFiltrada() {
    document.getElementById("warning").innerHTML = "";

    fetch("http://localhost:8080/html/administrador/adminListProd.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {

                if (filtrado.value == 1) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre contratante" + "</td><td>" + "# de Identificación" + "</td><td>" + "Nombre producto" + "</td><td>" + "Tipo" + "</td><td>" + "Fotografía" + "</td><td>" + "Monto" + "</td><td>" + "Categoría" + "</td><td>" + "Estado" + "</td><td>" + "Editar" + "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    entrar = false;
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            entrar = true;
                            break;
                        }
                    }
                    if(entrar == true){
                        var encontroActivo = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].estado == "Activo"){
                                encontroActivo = true;
                                break;
                            }
                        }

                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroActivo == true) {
                                if (json[cont].estado == "Activado" && json[cont].tipo == "Producto") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreContratante + "</td><td>" + json[cont].numeroIdentificacionContratante + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><td>" + json[cont].estado + "</td><td>" + "<i class='fas fa-edit'>" + "</i>" + "</td></tr>"
                                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaProductosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }

                    }else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }
                    

                } else if (filtrado.value == 2) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre contratante" + "</td><td>" + "# de Identificación" + "</td><td>" + "Nombre producto" + "</td><td>" + "Tipo" + "</td><td>" + "Fotografía" + "</td><td>" + "Monto" + "</td><td>" + "Categoría" + "</td><td>" + "Estado" + "</td><td>" + "Editar" + "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        entrar = false;
                        if (json[cont].tipo == "Producto" && json[cont].estado == "Inactivado" ) {
                            entrar = true;
                            break;
                        }
                    }

                    if (entrar == true) {
                        var encontroInactivo = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].estado == "Inactivado"){
                                encontroInactivo = true;
                                break;
                            }
                        }
                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroInactivo == true) {
                                if (json[cont].estado == "Inactivado" && json[cont].tipo == "Producto") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreContratante + "</td><td>" + json[cont].numeroIdentificacionContratante + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><td>" + json[cont].estado + "</td><td>" + "<i class='fas fa-edit'>" + "</i>" + "</td></tr>"
                                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaProductosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }
                    } else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                } else if (filtrado.value == 3) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre contratante" + "</td><td>" + "# de Identificación" + "</td><td>" + "Nombre producto" + "</td><td>" + "Tipo" + "</td><td>" + "Fotografía" + "</td><td>" + "Monto" + "</td><td>" + "Categoría" + "</td><td>" + "Estado" + "</td><td>" + "Editar" + "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            entrar = true;
                            break;
                        }
                    }
                    if (entrar == true) {
                        for (var cont = 0; json.length > cont; cont++) {
                            if (json[cont].tipo == "Producto") {
                                var linea = '<tr class="publicidad1"><td>' + json[cont].nombreContratante + "</td><td>" + json[cont].numeroIdentificacionContratante + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><td>" + json[cont].estado + "</td><td>" + "<i class='fas fa-edit'>" + "</i>" + "</td></tr>"
                                document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                            }

                        }


                    } else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                }


            }
        )
}