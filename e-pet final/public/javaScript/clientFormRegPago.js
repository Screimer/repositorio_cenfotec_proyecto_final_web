/*Aqui estamos guardando sobre cada variable de tipo consstante los id de cada campo 
ya sea de tipo select o de tipo text etc basicamente cada campo
donde el usuario esta guardando datos */
const form = document.getElementById("form");
const tipoTarjeta = document.getElementById("tipoTarjeta");
const titularTarjeta = document.getElementById("titularTarjeta");
const numeroTarjeta = document.getElementById("numeroTarjeta");
const codigoSeguridad = document.getElementById("codigoSeguridad");
const terminosCondiciones = document.getElementById("terminosCondiciones");



/*Aqui estamos guardando sobre cada variable nuevamente de tipo consstante 
los id de cada parrafo colocado debajo de cada text box select etc etc*/
const advertenciaTipoTarjeta = document.getElementById("warningTipoTarjeta");
const advertenciaTitularTarjeta = document.getElementById("warningTitularTarjeta");
const advertenciaNumeroTarjeta = document.getElementById("warningNumeroTarjeta");
const advertenciaCodigoSeguridad = document.getElementById("warningCodigoSeguridad");
const advertenciaCondiciones = document.getElementById("warningCondiciones");

/*Aqui creamos una funcion que nos permite detectar un boton de tipo submit
cada vez que se preciona el boton, la funcion detecta el llamado de submit
y comienza la respectiva validacion llevada a cabo en esta funcion*/ 
form.addEventListener("submit", e=>{
    e.preventDefault()
    
    let entrar = false

    let warningTipoTarjeta = ""
    let warningTitularTarjeta = ""
    let warningNumeroTarjeta = ""
    let warningCodigoSeguridad = ""
    let warningCondiciones = ""
    
    
    
    
    /*Validamos que el rango de caracteres sea igual o menor que 0
    para el nombre del producto*/ 
    if(tipoTarjeta.value == 0 ){
        warningTipoTarjeta = `Por favor seleccione un tipo de tarjeta. <br> <br>`
        entrar = true
    } else if (titularTarjeta.value.length <= 6){
        warningTitularTarjeta = `Titular de tarjeta muy pequeño. <br><br> `
        entrar = true
    } else if (numeroTarjeta.value.length < 16 ) {
        warningNumeroTarjeta = `Por favor digite un numero de tarjeta válido. <br> <br>`
        entrar = true            
    } else if (codigoSeguridad.value.length < 2){
        warningCodigoSeguridad = `Por favor digite un código de tarjeta válido. <br> <br> `
        entrar = true    
    } else if (terminosCondiciones.checked == false) {
        warningCondiciones = `Por favor acepte los términos y condiciones. <br> <br> `
        entrar = true
    }

       
    /*Aqui comienza las validaciones de la variable entrar, la cual nos permite 
    saber si existe un error en algun campo asignandole sobre cada if anteior el valor 
    de true, al solo colocar if(entrar) estamos validando rapidamente que la variable entrar
    tenga un valor a true es decir "Si entrar es igual a true entonces, eso quiere decir
    la proposicion anterior."*/ 
    if(entrar){
        advertenciaTipoTarjeta.innerHTML = warningTipoTarjeta
    } 

    if(entrar) {
        advertenciaTitularTarjeta.innerHTML = warningTitularTarjeta
    }

    if(entrar) {
        advertenciaNumeroTarjeta.innerHTML =  warningNumeroTarjeta
    }

    if(entrar) {
        advertenciaCodigoSeguridad.innerHTML = warningCodigoSeguridad
    }

    if(entrar) {
        advertenciaCondiciones.innerHTML = warningCondiciones
    }

    

    
    if(entrar == false) {
        advertenciaTipoTarjeta.innerHTML = ""
        advertenciaTitularTarjeta.innerHTML = ""
        advertenciaNumeroTarjeta.innerHTML = ""
        advertenciaCodigoSeguridad.innerHTML = ""
        advertenciaCondiciones.innerHTML = ""
    }

        
    /*Falta hacer la del sweetalert*/

})

function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}