/*Obtenemos en variables los valores de cada campo*/
const nombre = document.getElementById("name");
const email = document.getElementById("email");
const primerApellido = document.getElementById("primerApellido");
const segundoApellido = document.getElementById("segundoApellido");
const form = document.getElementById("form");

const tipoid = document.getElementById("tipoId");
const genero = document.getElementById("genero");
const direccionExacta = document.getElementById("direccionExacta");
const numeroTelefono = document.getElementById("numeroTelefono");
const numeroIdentificacion = document.getElementById("numeroIdentificacion");

/*Creamos variables y les asignamos el valor id de cada parrafo warning*/ 
const advertenciaNombre = document.getElementById("warningNombre");
const advertenciaPrimerApellido = document.getElementById("warningPrimerApellido");
const advertenciaSegundoApellido = document.getElementById("warningSegundoApellido");
const advertenciaCorreoElectronico = document.getElementById("warningCorreoElectronico");
const advertenciaGenero = document.getElementById("warningGenero");
const advertenciaTipoIdentificacion = document.getElementById("warningTipoIdentificacion");
const advertenciaNumeroIdentificacion = document.getElementById("warningNumeroIdentificacion");
const advertenciaDireccionExacta = document.getElementById("warningDireccionExacta");
const advertenciaNumeroTelefono = document.getElementById("warningNumeroTelefono");

form.addEventListener("submit", e=>{
    e.preventDefault()
    
    let warningNombre = ""
    let warningPrimerApellido = ""
    let warningSegundoApellido = ""
    let warningCorreoElectronico = ""
    let warningGenero = ""
    let warningTipoIdentificacion = ""
    let warningNumeroIndentificacion = ""
    let warningDireccionExacta = ""
    let warningNumeroTelefono = ""
    let entrar = false
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/

    
    if(nombre.value.length <= 0 ){
        warningNombre = `El nombre no es valido <br>`
        entrar = true
    } else if (primerApellido.value.length <= 0 ){
        warningPrimerApellido = `El apellido1 no es valido <br>`
        entrar = true
    } else if (segundoApellido.value.length <= 0){
        warningSegundoApellido = `El apellido2 no es valido <br>`
        entrar = true
    } else if (!regexEmail.test(email.value)) {
        warningCorreoElectronico = `El email no es valido <br>`
        entrar = true         
    } else if (genero.value == 0){
        warningGenero = `Tipo de genero vacío  <br>`
        entrar = true
    } else if (tipoid.value == 0) {
        warningTipoIdentificacion = `Tipo id vacío  <br>`
        entrar = true
    } else if(numeroIdentificacion.value.length  < 9 ){
        warningNumeroIndentificacion = `Numero de identificación inválido  <br>`
        entrar = true
    } else if(direccionExacta.value.length  < 7 ){
        warningDireccionExacta = `Dirección muy pequeña   <br>`
        entrar = true
    } else if(numeroTelefono.value.length  < 8 ){
        warningNumeroTelefono = `Numero de teléfono inválido   <br>`
        entrar = true
    }       
            
    
        
   

    
    if(entrar) {
        advertenciaNombre.innerHTML = warningNombre
    }

    if(entrar) {
        advertenciaPrimerApellido.innerHTML = warningPrimerApellido
    }

    if(entrar) {
        advertenciaSegundoApellido.innerHTML = warningSegundoApellido
    }

    if(entrar) {
        advertenciaCorreoElectronico.innerHTML = warningCorreoElectronico
    }

    if(entrar) {
        advertenciaGenero.innerHTML = warningGenero
    }

    if(entrar) {
        advertenciaTipoIdentificacion.innerHTML = warningTipoIdentificacion
    }

    if(entrar) {
        advertenciaNumeroIdentificacion.innerHTML = warningNumeroIndentificacion
    }

    if(entrar) {
        advertenciaDireccionExacta.innerHTML = warningDireccionExacta
    }

    if(entrar) {
        advertenciaNumeroTelefono.innerHTML = warningNumeroTelefono
    }

    

    

    

    if(entrar) {
        advertenciaNumeroIdentificacion.innerHTML = warningNumeroIndentificacion
    }

    
    if(entrar == false){
        advertenciaNombre.innerHTML = ""
        advertenciaPrimerApellido.innerHTML = ""
        advertenciaSegundoApellido.innerHTML = ""
        advertenciaCorreoElectronico.innerHTML = ""
        advertenciaGenero.innerHTML = ""
        advertenciaTipoIdentificacion.innerHTML = ""
        advertenciaNumeroIdentificacion.innerHTML = ""
        advertenciaDireccionExacta.innerHTML = ""
        advertenciaNumeroTelefono.innerHTML = ""
    }else{
        
    }



})

function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46 , "@"],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}