const mensajeAlerta = "No hay suficientes productos registrados para mostrar en esta categoría";
function cargarProductos() {
    fetch("http://localhost:8080/ListaProductos/")
        .then(
            function (response) {
                console.log(response);
                return response.json();
            }
        )
        .then(
            function (json) {

                for (var cont = 0; json.length > cont; cont++) {

                    if (json[cont].especie == "Perros" && json[cont].tipo == "Producto") {
                        document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                        document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                        document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                        document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                    }
                }
            }
        )
}



function FiltrarProductosProvincia() {
    fetch("http://localhost:8080/ListaProductos/")
        .then(
            function (response) {
                console.log(response);
                return response.json();
            }
        )
        .then(
            function (json) {

                if (document.getElementById("provincias").options[provincias.selectedIndex].text == "") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }
                    for (var cont = 0; json.length > cont; cont++) {

                        if (json[cont].especie == "Perros" && json[cont].tipo == "Producto") {
                            document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                            document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                            document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                            document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                        }
                    }
                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "San José") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "San José") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "San José") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }


                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Alajuela") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Alajuela") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Alajuela") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }


                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Cartago") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Cartago") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Cartago") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Heredia") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Heredia") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Heredia") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Liberia") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Liberia") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Liberia") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Puntarenas") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Puntarenas") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Puntarenas") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("provincias").options[provincias.selectedIndex].text == "Limón") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].provincias == "Limón") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].provincias == "Limón") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                }

            }
        )
}



function cargarCantones() {
    fetch("http://localhost:8080/ListaProductos/")
        .then(
            function (response) {
                console.log(response);
                return response.json();
            }
        )
        .then(
            function (json) {

                if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Pérez Zeledón") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Pérez Zeledón") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Pérez Zeledón") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Puriscal") {
                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Puriscal") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Puriscal") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }


                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Turrubares") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Turrubares") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Turrubares") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Dota") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Dota") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Dota") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Acosta") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Acosta") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Acosta") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Tarrazú") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Tarrazú") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Tarrazú") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Vázquez de Coronado") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Vázquez de Coronado") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Vázquez de Coronado") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Aserrí") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Aserrí") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Aserrí") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Mora") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Mora") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Mora") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "León Cortés") {


                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "León Cortés") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "León Cortés") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Desamparados") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Desamparados") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Desamparados") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Santa Ana") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Santa Ana") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Santa Ana") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "San José") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "San José") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "San José") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Escazú") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Escazú") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Escazú") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Goicoechea") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Goicoechea") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Goicoechea") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Moravia") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Moravia") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Moravia") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Alajuelita") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Alajuelita") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Alajuelita") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Curridabat") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Curridabat") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Curridabat") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Montes de Oca") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Montes de Oca") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Montes de Oca") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else if (document.getElementById("cantones").options[cantones.selectedIndex].text == "Tibás") {

                    for (var cont = 0; json.length > cont; cont++) {
                        document.getElementById("imagen" + cont).setAttribute("src", "");
                        document.getElementById("precio" + cont).innerHTML = "";
                        document.getElementById("descripcion" + cont).innerHTML = "";
                        document.getElementById("nombreProducto" + cont).innerHTML = "";
                    }

                    var existeProvincias = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].cantones == "Tibás") {
                            existeProvincias = true;
                            break
                        }
                    }

                    if (existeProvincias == true) {
                        for (var cont = 0; json.length > cont; cont++) {

                            if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" && json[cont].cantones == "Tibás") {
                                document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                                document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                                document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                                document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                            }
                        }
                    } else {
                        var maximo = 6;
                        for (var cont = 0; maximo > cont; cont++) {
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay suficientes productos registrados para mostrar en esta categoría";
                        }
                    }

                } else {
                    for (var cont = 0; json.length > cont; cont++) {

                        if (json[cont].especie == "Perros" && json[cont].tipo == "Producto") {
                            document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                            document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                            document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                            document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;

                        }
                    }
                }


            }
        )
}

/*Creacion del filtrado de la barra de busqueda*/
function buscadorInterno() {

    fetch("http://localhost:8080/ListaProductos/")
        .then(
            function (response) {
                console.log(response);
                return response.json();
            }
        )
        .then(
            function (json) {

                const barraDeBusqueda = document.getElementById('barraDeBusqueda');

                const texto = barraDeBusqueda.value.toLowerCase();
                var arreglo = [];
                for (var cont = 0; json.length > cont; cont++) {
                    arreglo.push(json[cont].nombreDelProducto.toLowerCase());
                }

                let nombre = arreglo;
                console.log(nombre);

                for (var cont = 0; json.length > cont; cont++) {

                    if (nombre[cont].indexOf(texto) != -1) {
                        for (var cont = 0; json.length > cont; cont++) {
                            document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/" + json[cont].nombreFoto);
                            document.getElementById("precio" + cont).innerHTML = "Precio:" + " " + "₡" + json[cont].precio;
                            document.getElementById("descripcion" + cont).innerHTML = json[cont].descripcion;
                            document.getElementById("nombreProducto" + cont).innerHTML = json[cont].nombreDelProducto;
                        }


                    } else {
                        for (var cont = 0; 6 > cont; cont++) {
                            document.getElementById("imagen" + cont).setAttribute("src", "../../imgs/fotosDeCategoriasOServicios/");
                            document.getElementById("precio" + cont).innerHTML = "";
                            document.getElementById("descripcion" + cont).innerHTML = "";
                            document.getElementById("nombreProducto" + cont).innerHTML = "No hay productos que coincidan con la búsqueda.";
                        }

                    }



                }

                if (texto.value == "") {
                    cargarProductos();
                                       

                }



            }
        )
}



