var filtrado = document.getElementById("filtrarStatus");


let respuesta = "";

function cargarTabla() {
    fetch("http://localhost:8080/html/cliente/clienteListarHistorialPedidos.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {


                let cedulaClienteGuardada = JSON.parse(localStorage.getItem("id"));
                var existenPedidosDelCliente = false;

                for (var cont = 0; json.length > cont; cont++) {
                    if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                        existenPedidosDelCliente = true;
                        break;
                    } else {
                        existenPedidosDelCliente = false;
                    }
                }

                if (existenPedidosDelCliente == true) {
                    for (var cont = 0; json.length > cont; cont++) {
                        if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                            if (json[cont].estado == "Entregado") {
                                respuesta = "<a class='calificar' href='clienteCalificarProv.html'>Calificar</a>";
                            } else {
                                respuesta = "No disponible";
                            }
                            var linea = '<tr class="publicidad1"><td>' + json[cont].nombreProducto + "</td><td>" + json[cont].cantidad + "</td><td>" + "₡" + json[cont].precioUnitario + "</td><td>" + "₡" + json[cont].total + "</td><td>" + json[cont].nombreProveedor + "</td><td>" + json[cont].numeroProveedor + "</td><td>" + json[cont].estado + "</td><td>" + respuesta + "</td><td>" + "<input type='button' value='boton de coralia'>" + "</td</tr>"
                            document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforeend", linea);
                        }

                    }
                } else {
                    document.getElementById("tablaMisPedidos").innerHTML = "";
                    document.getElementById("warning").innerHTML = "Su historial de pedidos se encuentra vacío.";
                }

            }
        )
}



function cargarTablaOrdenada() {

    fetch("http://localhost:8080/html/cliente/clienteListarHistorialPedidos.html/")
        .then(
            function (response) {
                return response.json();
            }
        )
        .then(
            function (json) {
                let cedulaClienteGuardada = JSON.parse(localStorage.getItem("id"));



                if (filtrado.value == 1) {
                    var table = document.getElementById("tablaMisPedidos").innerHTML = "";
                    var lineaPrincipal = '<table id="tablaMisPedidos" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Producto/Servicio" + "</td><td>" + "Cantidad" + "</td><td>" + "Precio unitario" + "</td><td>" + "Total" + "</td><td>" + "Nombre proveedor" + "</td><td>" + "Numero proveedor" + "</td><td>" + "Estado" + "</td><td>" + "Calificar" + "</td><td>" + "Cancelar mi pedido" + "</td</tr>" + '</table>'

                    document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforebegin", lineaPrincipal);

                    var existenPedidosDelCliente = false;

                    for (var cont = 0; json.length > cont; cont++) {
                        if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                            existenPedidosDelCliente = true;
                            break;
                        } else {
                            existenPedidosDelCliente = false;
                        }
                    }
                    if (existenPedidosDelCliente == true) {
                        for (var cont = 0; json.length > cont; cont++) {
                            if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                                if (json[cont].estado == "Entregado") {
                                    respuesta = "<a class='calificar' href='clienteCalificarProv.html'>Calificar</a>";
                                } else {
                                    respuesta = "No disponible";
                                }
                                if (json[cont].estado == "Entregado") {

                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreProducto + "</td><td>" + json[cont].cantidad + "</td><td>" + "₡" + json[cont].precioUnitario + "</td><td>" + "₡" + json[cont].total + "</td><td>" + json[cont].nombreProveedor + "</td><td>" + json[cont].numeroProveedor + "</td><td>" + json[cont].estado + "</td><td>" + respuesta + "</td><td>" + "<input type='button' value='boton de coralia'>" + "</td</tr>"

                                    document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforeend", linea);
                                }
                            }
                        }

                    } else {
                        document.getElementById("tablaMisPedidos").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Su historial de pedidos se encuentra vacío.";
                    }




                } else if (filtrado.value == 2) {
                    var existenPedidosDelCliente = false;
                    var table = document.getElementById("tablaMisPedidos").innerHTML = "";
                    var lineaPrincipal = '<table id="tablaMisPedidos" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Producto/Servicio" + "</td><td>" + "Cantidad" + "</td><td>" + "Precio unitario" + "</td><td>" + "Total" + "</td><td>" + "Nombre proveedor" + "</td><td>" + "Numero proveedor" + "</td><td>" + "Estado" + "</td><td>" + "Calificar" + "</td><td>" + "Cancelar mi pedido" + "</td</tr>" + '</table>'

                    document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforebegin", lineaPrincipal);

                    for (var cont = 0; json.length > cont; cont++) {
                        if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                            existenPedidosDelCliente = true;
                            break;
                        } else {
                            existenPedidosDelCliente = false;
                        }
                    }

                    if (existenPedidosDelCliente == true) {
                        for (var cont = 0; json.length > cont; cont++) {
                            if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                                if (json[cont].estado == "Entregado") {
                                    respuesta = "<a class='calificar' href='clienteCalificarProv.html'>Calificar</a>";
                                } else {
                                    respuesta = "No disponible";
                                }
                                if (json[cont].estado == "Pendiente") {

                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreProducto + "</td><td>" + json[cont].cantidad + "</td><td>" + "₡" + json[cont].precioUnitario + "</td><td>" + "₡" + json[cont].total + "</td><td>" + json[cont].nombreProveedor + "</td><td>" + json[cont].numeroProveedor + "</td><td>" + json[cont].estado + "</td><td>" + respuesta + "</td><td>" + "<input type='button' value='boton de coralia'>" + "</td</tr>"

                                    document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforeend", linea);
                                }
                            }
                        }
                    } else {
                        document.getElementById("tablaMisPedidos").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Su historial de pedidos se encuentra vacío.";
                    }

                } else if (filtrado.value == 0) {
                    var existenPedidosDelCliente = false;
                    document.getElementById("tablaMisPedidos").innerHTML = "";
                    var lineaPrincipal = '<table id="tablaMisPedidos" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Producto/Servicio" + "</td><td>" + "Cantidad" + "</td><td>" + "Precio unitario" + "</td><td>" + "Total" + "</td><td>" + "Nombre proveedor" + "</td><td>" + "Numero proveedor" + "</td><td>" + "Estado" + "</td><td>" + "Calificar" + "</td><td>" + "Cancelar mi pedido" + "</td</tr>" + '</table>'

                    document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforebegin", lineaPrincipal);

                    for (var cont = 0; json.length > cont; cont++) {
                        if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                            existenPedidosDelCliente = true;
                            break;
                        } else {
                            existenPedidosDelCliente = false;
                        }
                    }

                    if(existenPedidosDelCliente == true){
                        for (var cont = 0; json.length > cont; cont++) {
                            if (cedulaClienteGuardada.identificacion == json[cont].cedulaCliente) {
                                if (json[cont].estado == "Entregado") {
                                    respuesta = "<a class='calificar' href='clienteCalificarProv.html'>Calificar</a>";
                                } else {
                                    respuesta = "No disponible";
                                }
    
    
                                var linea = '<tr class="publicidad1"><td>' + json[cont].nombreProducto + "</td><td>" + json[cont].cantidad + "</td><td>" + "₡" + json[cont].precioUnitario + "</td><td>" + "₡" + json[cont].total + "</td><td>" + json[cont].nombreProveedor + "</td><td>" + json[cont].numeroProveedor + "</td><td>" + json[cont].estado + "</td><td>" + respuesta + "</td><td>" + "<input type='button' value='boton de coralia'>" + "</td</tr>"
                                document.getElementById("tablaMisPedidos").insertAdjacentHTML("beforeend", linea);
                            } 
    
    
                        }
                    } else{
                        document.getElementById("tablaMisPedidos").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Su historial de pedidos se encuentra vacío.";
                    }

                    


                }







            }
        )
}
















