

/*Aqui estamos guardando sobre cada variable de tipo consstante los id de cada campo 
ya sea de tipo select o de tipo text etc basicamente cada campo
donde el usuario esta guardando datos */

var imagen2 = null;
const form = document.getElementById("form");
const nombre = document.getElementById("nombreDelProducto");
const descripcion = document.getElementById("descripcion");
const precio = document.getElementById("precio");
const especie = document.getElementById("especie");
const tipo = document.getElementById("tipo");
const imagen = document.getElementById("imagen");

/*Aqui estamos guardando sobre cada variable nuevamente de tipo consstante 
los id de cada parrafo colocado debajo de cada text box select etc etc*/
const advertenciaNombreProducto = document.getElementById("warningNombreProducto");
const advertenciaDescripcionProducto = document.getElementById("warningDescripcionProducto");
const advertenciaPrecioDelProducto = document.getElementById("warningPrecioDelProducto");
const advertenciaSeleccionDeEspecie = document.getElementById("warningSeleccionDeEspecie");
const advertenciaTipo = document.getElementById("warningTipo");
const advertenciaImagen = document.getElementById("warning-imagen");

const terminarRegistro = document.getElementById("terminarRegistro");
var completo = 0;

/*Aqui creamos una funcion que nos permite detectar un boton de tipo submit
cada vez que se preciona el boton, la funcion detecta el llamado de submit
y comienza la respectiva validacion llevada a cabo en esta funcion*/ 
function formulario(){
    form.addEventListener("click", e=>{
        e.preventDefault()
        
        let respuestaFuncionNombre = validacionCampoNombre();
        let respuestaFuncionDescripcion = validacionCampoDescripcion();
        let respuestaFuncionPrecio = validacionCampoPrecio();
        let respuestaFuncionEspecie = validacionSelectEspecie();
        let respuestaFuncionTipo = validacionSelectTipo();
        let respuestaFuncionImagen = validacionSubirImagen();

        if(respuestaFuncionNombre == false && respuestaFuncionDescripcion == false && respuestaFuncionPrecio == false && respuestaFuncionEspecie == false &&  respuestaFuncionTipo == false && respuestaFuncionImagen == false && terminarRegistro.addEventListener('click', function (e){
            completo = 1; 
            insertarProductoOServicio(completo );
            
            
        }) );

        
        
    })
}

function insertarProductoOServicio(completo ) {
    if(completo == 1 ){
        
        
        
        
        var datosProdcutosOServicio = {
            nombreDelProducto: document.getElementById("nombreDelProducto").value,
            descripcion: document.getElementById("descripcion").value,
            precio: document.getElementById("precio").value,
            especie: document.getElementById("especie").options[especie.selectedIndex].text,
            tipo: tipoTexto,
            provincias: document.getElementById("provincias").options[provincias.selectedIndex].text,
            cantones: document.getElementById("cantones").options[cantones.selectedIndex].text,
            categoria: document.getElementById("categoria").options[categoria.selectedIndex].text,
            identificacionProveedor: document.getElementById("identificacionProveedor").value
            
        }
                        
           
            fetch("http://localhost:8080/html/proveedor/provFormRegProdyServ.html/insertar", {
            method: 'POST',
            body: JSON.stringify(datosProdcutosOServicio),
            headers: {'Content-Type': 'application/json'}
          })
            .then(
              function(response){
                return response.json();
            }
            
        )
        
    }
    
}


function readFile (input){
    imagen2  = input.files[0];
}

function subirArchivo(){
    var file = document.getElementById("imagen").files[0];
    console.log(document.getElementById("imagen").files[0]);
    let reader = new FileReader();

    reader.readAsArrayBuffer(file);
    var datosProdcutosOServicio = {
        archivo: imagen2
                
    }
        document.getElementById("form").submit();
        
        fetch("http://localhost:8080/html/proveedor/provFormRegProdyServ.html/subir", {
        method: 'POST',
        body: JSON.stringify(datosProdcutosOServicio),
        headers: {'Content-Type': 'application/json'}
      })
        .then(
          function(response){
            console.log(response);  
            return response;
            
        }

    )
}


function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}

function validacionCampoNombre(){
    let entrar = false
    let warningNombreProducto = ""

    if(nombre.value.length <= 0 ){
        warningNombreProducto = `Por favor digite un nombre.`
        entrar = true
    }    

    if(entrar == true){
        advertenciaNombreProducto.innerHTML = warningNombreProducto
        
            
    } else{
        advertenciaNombreProducto.innerHTML = ""
    } 

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionCampoDescripcion(){
    let entrar = false
    let warningDescripcionProducto = ""

    if (descripcion.value.length <= 2){
        warningDescripcionProducto = `Por favor rellene la descripción. `
        entrar = true
    }    

    if(entrar) {
        advertenciaDescripcionProducto.innerHTML = warningDescripcionProducto
    } else{
        advertenciaDescripcionProducto.innerHTML = ""
    }

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionCampoPrecio(){
    let entrar = false
    let warningPrecioDelProducto = ""

    if (precio.value.length <= 0 ) {
        warningPrecioDelProducto = `Por favor digite un precio.`
        entrar = true
    }     
    
    if(entrar) {
        advertenciaPrecioDelProducto.innerHTML = warningPrecioDelProducto
    } else{
        advertenciaPrecioDelProducto.innerHTML = ""
    }

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionSelectEspecie(){
    let entrar = false
    let warningSeleccionDeEspecie = ""

    if (especie.value <= 0){
        warningSeleccionDeEspecie = `Por favor seleccione una especie válida.`
        entrar = true
    }
    
    if(entrar) {
        advertenciaSeleccionDeEspecie.innerHTML = warningSeleccionDeEspecie
    } else{
        advertenciaSeleccionDeEspecie.innerHTML = ""
    }

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionSelectTipo() {
    let entrar = false
    let warningTipo = ""

    if (tipo.value <= 0) {
        warningTipo = `Por favor seleccione un tipo válido.`
        entrar = true
    }

    if(entrar) {
        advertenciaTipo.innerHTML = warningTipo
    } else{
        advertenciaTipo.innerHTML = ""
    }

    if (entrar == false) {
        return false;
    } else{
        return true;
    }

}

function validacionSubirImagen(){
    let entrar = false
    let warningImagen = ""

    if (imagen.value == ""){
        warningImagen = "Por favor seleccione una imagen"
        entrar = true
    }

    if(entrar) {
        advertenciaImagen.innerHTML = warningImagen
    } else{
        advertenciaImagen.innerHTML = ""
    }

    if(entrar == false){
        return false;
    } else{
        return true;
    }


}


function cargarTablaConCategorias() {
    fetch("http://localhost:8080/html/proveedor/provFormRegProdyServ.html/categoriasRegistradas")
        .then(
            function (response) {
                return response.json();
                
            }
        )
        .then(
            function (json) {
                
                var entrar = true;
                
                for (var cont = 0; json.length > cont; cont++) {
                    if (json.length == 0) {
                        entrar = false;
                        break;
                    }
                }
                if (entrar == true) {
                    for (var cont = 0; json.length > cont; cont++) {
                        /*
                        var linea = '<option>' + json[cont].nombre + "</option>"
                        select.innerHTML("linea");
                        */
                        var select = document.getElementById("categoria"),
                        txtVal = json[cont].nombre,
                        newOption = document.createElement("OPTION"),
                        newOptionVal = document.createTextNode(txtVal);
                        newOption.appendChild(newOptionVal);
                        select.insertBefore(newOption, select.lastChild);

                    }
                } else {
                    var select = document.getElementById("categoria"),
                        txtVal = "Sin registros en la base de datos",
                        newOption = document.createElement("OPTION"),
                        newOptionVal = document.createTextNode(txtVal);
                        newOption.appendChild(newOptionVal);
                        select.insertBefore(newOption, select.lastChild);
                }


            }
        )
}




