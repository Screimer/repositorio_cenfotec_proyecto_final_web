/*Jalamos los campos mediante el id*/

const campoEmail = document.getElementById("email");
const campoPassword = document.getElementById("password");
const form = document.getElementById("form");

/*Cuentas posibles para el cliente o simulacion de la base de datos*/

const correoCliente = "cliente@gmail.com"
const contrasenaCliente = "123456789"
/*Cuentas posibles o para el proveedor simulacion de la base de datos*/ 
const correoProveedor = "proveedor@gmail.com"
const contrasenaProveedor = "123456789"

/*Cuentas posibles o somulacion de la bse de datos para el administrador*/ 
const correoAdministrador = "administrador@gmail.com"
const contrasenaAdministrador = "123456789"
/*Finalizacion de la posible simulacion de la base de datos*/

/*Jalamos el parrafo donde va a tirar la alerta*/
const parrafo = document.getElementById("validacionInicioSesion");
/*Cerramos la llamada*/ 

form.addEventListener("submit", e=>{
    e.preventDefault()


    let dependiendoCuenta = ""
    /*Esta variable nos va a pertmir validar a mas a fondo luego ya que 
    la vamos a utilizar para desde el punto de vist a*/ 
    let validacionInicioSesion = true

          
    if (campoEmail.value == correoCliente && campoPassword.value  == contrasenaCliente) {
        dependiendoCuenta = "cliente"
        validacionInicioSesion = true
        inicioSesion(dependiendoCuenta );

    } else if (campoEmail.value == correoProveedor && campoPassword.value == contrasenaProveedor){
        dependiendoCuenta = "proveedor"
        validacionInicioSesion = true
        inicioSesion(dependiendoCuenta );
    } else if (campoEmail.value == correoAdministrador && campoPassword.value == contrasenaAdministrador){
        dependiendoCuenta = "administrador"
        validacionInicioSesion = true
        inicioSesion(dependiendoCuenta );
    } 
   
})

function inicioSesion(dependiendoCuenta){
    
    switch (dependiendoCuenta) {
        case "cliente":
            location.href = "../../html/cliente/clienteHome.html"
            validacionInicioSesion = false
            break;
        case "proveedor":
            location.href = "../../html/proveedor/provHome.html"
            validacionInicioSesion = false
            break;
        case "administrador":
            location.href = "../../html/administrador/adminHome.html"  
            validacionInicioSesion = false
            break;
        default:
            parrafo.innerHTML = "No existen cuentas con estas credenciales <br> <br> "
            break;
    }

    
        
}



