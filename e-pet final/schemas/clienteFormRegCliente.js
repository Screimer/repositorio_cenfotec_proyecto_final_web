var mongoose = require('mongoose');


var crearClientesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cuenta:String,
    nombre: String,
    primerApellido: String,
    segundoApellido: String,
    email: String,
    genero: String,
    tipo: String,
    identificacion: String,
    fecha: String,
    direccionExacta: String,
    numeroTelefono: String,
    contrasena:String,
});
module.exports = mongoose.model('Cliente', crearClientesSchema, 'Clientes');