var mongoose = require('mongoose');

var adminPadecimientosSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  nombreDelPadecimiento: String,
  tipoAnimal: String
});

module.exports = mongoose.model('RegistroPadecimientoAdministrador', adminPadecimientosSchema, 'RegistroPadecimientosAdministrador');