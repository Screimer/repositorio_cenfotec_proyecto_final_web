var mongoose = require('mongoose');


var provFormRegProdyServSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombreContratante: String,
    numeroIdentificacionContratante: Number,
    nombreDelProducto: String,
    nombreFoto: String,
    descripcion: String,
    precio: Number,
    especie: String,
    tipo: String,
    categoria: String,
    identificacionProveedor: String,
    provincias:String,
    cantones:String,
    path: String

});
/*En el segundo string es la coleccion a la que yo quiero ingresar los datos.*/ 
module.exports = mongoose.model('ProductoYServicio', provFormRegProdyServSchema, 'ProductosYServicios');